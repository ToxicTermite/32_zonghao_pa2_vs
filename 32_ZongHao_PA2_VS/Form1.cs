﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _32_ZongHao_PA2_VS {
    public partial class Form1 : Form {
        int FirstNumber, SecondNumber, Sum;

        private void button2_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            FirstNumber = int.Parse(textBox1.Text);
            SecondNumber = int.Parse(textBox2.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of two numbers is " + Sum.ToString());
        }
    }
}
